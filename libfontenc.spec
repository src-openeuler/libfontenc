%define _x11fontdir		%{_datadir}/X11/fonts

Name:                     libfontenc
Version:                  1.1.8
Release:                  1
License:                  MIT
URL:                      https://www.x.org
Source0:                  https://xorg.freedesktop.org/releases/individual/lib/%{name}-%{version}.tar.gz
Summary:                  X.Org X11 libfontenc runtime library
BuildRequires:            pkgconfig xorg-x11-util-macros xorg-x11-proto-devel zlib-devel xorg-x11-font-utils

%description
X.Org X11 libfontenc runtime library

%package devel
Summary:                  X.Org X11 libfontenc development package
Requires:                 %{name} = %{version}-%{release}

%description devel
X.Org X11 libfontenc development package

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="$RPM_OPT_FLAGS -Os"
%configure --with-fontrootdir=%{_x11fontdir}
%make_build

%install
%make_install
%delete_la_and_a
%ldconfig_scriptlets

%check
make check

%files
%defattr(-,root,root)
%license COPYING
%{_libdir}/libfontenc.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/X11/fonts/fontenc.h
%{_libdir}/libfontenc.so
%{_libdir}/pkgconfig/fontenc.pc

%files help
%defattr(-,root,root)
%doc README.md

%changelog
* Tue Mar 12 2024 liweigang <liweiganga@uniontech.com> - 1.1.8-1
- update to version 1.1.8

* Sat Jul 22 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.1.7-1
- update 1.1.7

* Thu Nov 03 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.1.6-1
- update 1.1.6

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.4-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Sat Sep 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.4-1
- Package init
